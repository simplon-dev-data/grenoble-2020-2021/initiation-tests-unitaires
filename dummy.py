def dummy_function(value: int) -> str:
    if value < 0:
        raise ValueError()
    return str(value)
