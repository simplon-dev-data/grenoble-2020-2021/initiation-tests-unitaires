import pytest

from dummy import dummy_function


def test_dummy_function_success():
    assert dummy_function(12) == "12"


def test_dummy_function_failure():
    with pytest.raises(ValueError):
        dummy_function(-12)
