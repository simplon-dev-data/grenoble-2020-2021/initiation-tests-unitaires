# Initiation aux tests unitaires

> Découvrir le concept de test unitaire

## Recherche d'information

En groupe, rechercher des axes de réponses aux questions suivantes :

- [ ] Qu'est-ce qu'un _test unitaire_ ? Quel est son but ?
- [ ] Quels autres types de test existe-t-il en programmation ? Quelles sont les différences ?
- [ ] Quels outils permettent de tester du code Python ?
- [ ] En quoi les tests unitaires peuvent être utiles dans le domaine de la data ?

## Mise en place

Dans un environnement virtuel Python, installer le paquet Python `pytest`.

Si vous utiliser `pipenv`, vous pouvez directement lancer `pipenv install -d`
dans ce projet.

Vérifier que l'environnement est fonctionnel avec la commande suivante :

```bash
pytest -v tests
```

Vous venez d'exécuter votre première suite de tests unitaires !

Allez explorer les fichiers source Python suivants :

- [`dummy.py`](dummy.py)
- [`test_dummy.py`](tests/test_dummy.py)

### TODO

- [ ] Que contient `dummy.py` ?
- [ ] Que contient `test_dummy.py` ?
- [ ] Changer le résultat de la fonction `dummy_function` en multipliant
      par 2 la valeur avant la conversion en chaine de caractère
- [ ] Que se passe-t-il si on ré-exécute la suite de tests unitaire ?
- [ ] Comment résoudre la situation ?

## Echauffement

Nous allons commencer doucement cette initiation aux tests unitaires !

**Objectif** :

- Créer une fonction qui valide le format d'une adresse e-mail
- La fonction doit prendre en entrée une adresse e-mail sous forme de chaine de caractères
  et retourner le résultat de validation sous forme de booléen

### TODO

- [ ] Créer le fichier `emails.py`
- [ ] Créer le fichier de test `tests/test_emails.py`
- [ ] Implémenter la fonction `validate_email` dans le module `emails.py`
- [ ] Implémenter et vérifier le test `test_validate_email_success` dans le module `tests/test_emails.py`
- [ ] Implémenter et vérifier le test `test_validate_email_failure` dans le module `tests/test_emails.py`

## Prise de hauteur

Dans le domaine de la data, le code à tester intéragit très souvent avec l'environnement
extérieur (fichiers plats, APIs, bases de données, etc). Voyons comment définir
un environnement de test pour les données.

**Objectif** :

- Créer une fonction qui lit un fichier CSV et le charge dans une table d'une
  base de données SQLite
- Chaque test doit pouvoir être exécuté indépendemment des autres afin de garantir
  la _reproductibilité_
- Le(s) contexte(s) de test doivent être clairement définis

### TODO

- [ ] Qu'est-ce qu'une _fixture_ ?
- [ ] À quoi pourrait nous être utile une _fixture_ ?
- [ ] Créer un fichier `users.py` avec le contenu suivant :

```python
import csv
import sqlite3


def load_csv(filepath: str, conn: sqlite3.Connection):
    with open(filepath, "r") as f:
        reader = csv.DictReader(f)
        rows = [(r["first_name"], r["last_name"], r["email"]) for r in reader]

        cur = conn.cursor()
        cur.executemany(
            """
            INSERT INTO users (first_name, last_name, email)
            VALUES (?, ?, ?)
            """,
            rows,
        )
        conn.commit()
```

- [ ] Créer le fichier de test `tests/test_users.py` avec le contenu suivant :

```python
import pathlib
import sqlite3
import pytest
import users


@pytest.fixture(scope="session")
def db():
    conn = sqlite3.connect(":memory:")
    yield conn
    conn.close()


@pytest.fixture(scope="function")
def users_table(db):
    cur = db.cursor()
    cur.execute(
        """
        CREATE TABLE users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            first_name TEXT,
            last_name TEXT,
            email TEXT
        )
        """
    )
    db.commit()
    yield
    cur.execute("""DROP TABLE users""")
    db.commit()


def test_load_csv_success(db, users_table):
    filepath = pathlib.Path(__file__).parent / "data" / "users.csv"

    users.load_csv(filepath, db)

    cur = db.cursor()
    cur.execute("SELECT * FROM users")
    rows = cur.fetchall()
    assert len(rows) == 3
```

- [ ] Créer le fichier de données CSV `tests/data/users.csv` avec le contenu suivant :

```csv
first_name,last_name,email
John,Doe,john@doe.com
Jane,Doe,jane@doe.com
James,Doe,james@doe.com
```

- [ ] Vérifier que le test de chargement du fichier CSV fonctionne, plusieurs fois consécutives

## Approfondissement

Maintenant que nous savons tester le chargement de données dans une base SQLite
depuis un fichier CSV, nous pouvons tester des requêtes d'exploitation de données !

**Objectifs** :

- Créer une fonction qui compte le nombre d'utilisateur en base
- Créer une fonction qui retourne toutes les adresses e-mail en base
- Créer une fonction qui retourne le nom et prénom d'un utilisateur à partir de son e-mail

### TODO

- [ ] Ecrire chaque fonction
- [ ] Pour chaque fonction, écrire au moins 2 tests (une réussite et un échec)
